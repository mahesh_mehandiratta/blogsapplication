import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { IUser } from '../models/User';

const USERS_URL = 'http://localhost:3000/users/';
const BASE_URL_ID = 'http://localhost:3000/id/';
const header = {headers: new Headers({'Content-Type': 'application/json'})};

@Injectable()
export class AuthService {
  constructor(private http: Http) {
  }

  login(userId: string) {
    return this.http.get(USERS_URL + '/' + userId)
      .map(res => res.json());
  }
}
