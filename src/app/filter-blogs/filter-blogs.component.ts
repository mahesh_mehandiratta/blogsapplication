import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter-blogs',
  templateUrl: './filter-blogs.component.html',
  styleUrls: ['./filter-blogs.component.css']
})
export class FilterBlogsComponent {
  filterValue = 'Filter';
  selectedRadioButtonValue = 'All';

  @Output()
  radioButtonSelectionChanged: EventEmitter<string> =
    new EventEmitter<string>();

  onRadioButtonSelectionChange() {
    this.filterValue = this.selectedRadioButtonValue;
    this.radioButtonSelectionChanged
      .emit(this.selectedRadioButtonValue);
  }

  constructor() { }
}
