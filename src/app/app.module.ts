import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { NewBlogComponent } from './new-blog/new-blog.component';
import { EditBlogComponent } from './edit-blog/edit-blog.component';
import { ListBlogsComponent } from './list-blogs/list-blogs.component';

import {BlogService} from './services/app.service';
import { FilterBlogsComponent } from './filter-blogs/filter-blogs.component';
import { ReadBlogComponent } from './read-blog/read-blog.component';
import { PageNotFoundComponentComponent } from './page-not-found-component/page-not-found-component.component';
import { LoginPageComponent } from './login-page/login-page.component';
import {AuthService} from './services/auth.service';
import { BlogsDisplayComponent } from './blogs-display/blogs-display.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginPageComponent },
  { path: 'blogs', component: BlogsDisplayComponent },
  { path: 'add', component: NewBlogComponent },
  { path: 'edit/:id', component: EditBlogComponent },
  { path: 'read/:id', component: ReadBlogComponent },
  { path: '', redirectTo: '/blogs', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponentComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NewBlogComponent,
    EditBlogComponent,
    ListBlogsComponent,
    FilterBlogsComponent,
    ReadBlogComponent,
    PageNotFoundComponentComponent,
    LoginPageComponent,
    BlogsDisplayComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [BlogService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
