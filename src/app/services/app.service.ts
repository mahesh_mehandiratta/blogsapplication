import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

const BASE_URL = 'http://localhost:3000/blogs/';
const BASE_URL_ID = 'http://localhost:3000/newBlogID/';
const BASE_URL_USERS = 'http://localhost:3000/users/';
const header = {headers: new Headers({'Content-Type': 'application/json'})};

@Injectable()
export class BlogService {
  id = -1;
  constructor(private http: Http) {
  }

  loadBlogs() {
    return this.http.get(BASE_URL)
      .map(res => res.json());
  }
  getIdFromDB() {
    return this.http.get(BASE_URL_ID)
      .map(res => res.json());
  }
  getId(): number {
    return this.id;
  }
  setId(id) {
    this.id = id;
  }
  incrementId(): any {
    this.id++;
    return this.http.patch(BASE_URL_ID, {'newBlogID': this.getId()}, header)
       .map(res => res.json());
  }

  getBlogById(receivedId: string) {
    return this.http.get(BASE_URL + '/' + receivedId)
      .map(res => res.json());
  }

  postNewBlog(data) {
    return this.http.post(BASE_URL, data, header)
      .map(res => res.json());

  }

  updateData(data) {
    return this.http.patch(`BASE_URL${data.id}`, data, header)
      .map(res => res.json());
  }

  updateUser(data) {
    return this.http.patch(BASE_URL_USERS + `${data.id}`, data, header)
      .map(res => res.json());
  }

  checkData(data) {
    return data.id ? this.updateData(data) : this.postNewBlog(data)
      .map(res => res.json());
  }

  getUserById(userId) {
    return this.http.get(BASE_URL_USERS + '/' + userId)
      .map(res => res.json());
  }

  deleteBlog(blogID) {
    return this.http.delete(BASE_URL + `${blogID}`, header)
      .map(res => res.json());
  }

  updateBlog(data) {
    return this.http.patch(BASE_URL + `${data.id}`, data, header)
      .map(res => res.json());
  }
}
