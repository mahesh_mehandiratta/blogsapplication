import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BlogService} from '../services/app.service';
import {IBlog} from '../models/Blog';

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.css']
})
export class EditBlogComponent implements OnInit {

  allCategories: string[] = ['Life', 'Technology', 'Money', 'Health', 'Productivity', 'Medical'];

  blog: IBlog = {
    id: '',
    authorID: '',
    logo: '',
    content: '',
    category: '',
    author: '',
    title: '',
    date: ''
  };

  constructor(private blogService: BlogService,
              private _activatedRoute: ActivatedRoute, private _router: Router) {
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === '') {
      this._router.navigate(['/login']);
    } else {
      const blogId: string = this._activatedRoute.snapshot.params['id'];
      document.getElementById('loader').style.display = 'block';
      this.blogService.getBlogById(blogId)
        .subscribe((data) => {
          document.getElementById('loader').style.display = 'none';
          this.blog = data;
        });
    }
  }

  isCurrentUsersBlog(blogID): boolean {
    return (JSON.parse(window.localStorage.getItem('currentUser')).id === blogID);
  }

  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.blog.logo = btoa(binaryString);
  }

  onSubmit(): void {
    this.blogService.updateBlog(this.blog)
      .subscribe(data => {
        this.blogService.incrementId()
          .subscribe(() => {
            this._router.navigate(['/blogs']);
          });
      });
  }
}
