import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'BlogsApp';
  windowObject = window;
  constructor(private router: Router) { }


  logout() {
    localStorage.setItem('currentUser', '');
    this.router.navigate(['/login']);
  }
}
