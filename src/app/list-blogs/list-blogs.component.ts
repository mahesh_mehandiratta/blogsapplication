import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BlogService} from '../services/app.service';
import {IBlog} from "../models/Blog";

@Component({
  selector: 'app-list-blogs',
  templateUrl: './list-blogs.component.html',
  styleUrls: ['./list-blogs.component.css']
})

export class ListBlogsComponent implements OnInit {
  @Input()
  blogList: IBlog[] = [];
  blogIDBeingDeleted = '';
  selectedBlogRadioButton = 'All';

  constructor(private blogservice: BlogService, private _router: Router) {
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === '') {
      this._router.navigate(['/login']);
    } else {
      this.blogList = [];
    }
  }

  loadBlogs() {
    this.blogservice.loadBlogs()
      .subscribe((data) => {
        this.blogList = data;
      });
  }

  onBlogRadioButtonChange(selectedRadioButtonValue: string): void {
    this.selectedBlogRadioButton = selectedRadioButtonValue;
  }


  isFavorite(blogID): boolean {
    return JSON.parse(window.localStorage.getItem('currentUser')).favorites[blogID];
  }


  isCurrentUsersBlog(blogID): boolean {
    return (JSON.parse(window.localStorage.getItem('currentUser')).id === blogID);
  }

  deletePopup(blogID): void {
    this.blogIDBeingDeleted = blogID;
    // open
    document.getElementById('deleteBlogModalButton').click();
  }

  deleteBlog() {
    this.blogservice.deleteBlog(this.blogIDBeingDeleted).subscribe(
      (data) => {
        // close
        document.getElementById('deleteBlogModalButton').click();
        this.loadBlogs();
      });
  }
}
