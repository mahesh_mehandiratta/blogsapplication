import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import {IUser} from '../models/User';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  model: IUser = {
    id: '',
    username: '',
    password: '',
    favorites: ['']
  };
  errorMessage: string;

  constructor(private router: Router, private authService: AuthService) {}

  ngOnInit() { }

  login() {
    document.getElementById('loader').style.display = 'block';
    this.authService.login(this.model.username)
      .subscribe(
        data => {
          document.getElementById('loader').style.display = 'none';
          if (data == null) {
            this.errorMessage = 'Wrong username';
          } else if (data.password !== this.model.password) {
            this.errorMessage = 'Wrong password';
          } else {
            localStorage.setItem('currentUser', JSON.stringify(data));
            this.router.navigate(['/blogs']);
          }
        },
        error => {
          document.getElementById('loader').style.display = 'none';
          this.errorMessage = 'Wrong username';
        });
  }
}
