import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BlogService} from '../services/app.service';
import {IBlog} from '../models/Blog';

@Component({
  selector: 'app-blogs-display',
  templateUrl: './blogs-display.component.html',
  styleUrls: ['./blogs-display.component.css']
})
export class BlogsDisplayComponent implements OnInit {

  blogList: IBlog[];
  blogListCopy: IBlog[];
  titleValueSearched = '';

  constructor(private blogservice: BlogService, private _router: Router) {
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === '') {
      this._router.navigate(['/login']);
    } else {
      this.loadBlogs('All');
    }
  }

  loadBlogs(blogsToLoad) {
    document.getElementById('loader').style.display = 'block';
    this.blogservice.loadBlogs()
      .subscribe((data) => {
        this.blogList = data;

        if (blogsToLoad === 'favorites') {
          this.blogList = this.blogList.filter(
            blog => JSON.parse(window.localStorage.getItem('currentUser')).favorites[blog.id]
          );
        }

        if (blogsToLoad === 'myBlogs') {
          this.blogList = this.blogList.filter(
            blog => (JSON.parse(window.localStorage.getItem('currentUser')).id === blog.authorID)
          );
        }
        // copy blogList to blogListCopy so as to use in searching
        this.blogListCopy = this.blogList.slice();
        document.getElementById('loader').style.display = 'none';
      });
  }

  searchBasedOnTitle() {
    this.blogList = this.blogListCopy.filter(
      blog => {
        return blog.title.toLowerCase().indexOf(this.titleValueSearched.toLowerCase()) !== -1;
      }
    );
  }
}
