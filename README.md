# BlogApplication

* This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.9.
* Access full documentation here: https://drive.google.com/open?id=1_nAQdk3nBiZbm0YWjtsmkz440RKRgCAt 
* Use hosted version here: https://damp-spire-28174.herokuapp.com/
* See hosted json-server here: https://json-server-for-blogs.herokuapp.com/

## Development server

* Run `npm start` to start application. Navigate to `http://localhost:4200/`. 
* Login using one of the following 2 existing users:
* a. UserID: mahesh.ratta95@gmail.com
*	 Password: “1234”
* b. UserID: deveesh97@gmail.com 
*	 Password: “1234”
* You're ready to use all the features

## Completion

All funcationalities have been made except optional feature of using Change detection to improve efficiency of app.

## Endpoints made
1.	http://localhost:4200/	 : Points to the home page
2.	http://localhost:4200/blogs   : Same as above 1.
3.	http://localhost:4200/add      : Page to post a new blog
4.	http://localhost:4200/read/:id: Read a specific blog
5.	http://localhost:4200/edit/:id  : Edit a specific blog

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
