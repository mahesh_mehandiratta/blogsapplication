import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BlogService} from '../services/app.service';
import {IBlog} from '../models/Blog';

@Component({
  selector: 'app-read-blog',
  templateUrl: './read-blog.component.html',
  styleUrls: ['./read-blog.component.css']
})
export class ReadBlogComponent implements OnInit {

  blog: IBlog = {
    id: '',
    authorID: '',
    logo: '',
    content: '',
    category: '',
    author: '',
    title: '',
    date: ''
  };

  constructor(private blogService: BlogService,
              private _activatedRoute: ActivatedRoute, private _router: Router) {
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === '') {
      this._router.navigate(['/login']);
    } else {
      const blogId: string = this._activatedRoute.snapshot.params['id'];
      // this.blog = this.blogService.getEmployeeById(blogId);
      document.getElementById('loader').style.display = 'block';
      this.blogService.getBlogById(blogId)
        .subscribe((data) => {
          document.getElementById('loader').style.display = 'none';
          this.blog = data;
        });
    }
  }


  isFavorite(blogID): boolean {
    return JSON.parse(window.localStorage.getItem('currentUser')).favorites[blogID];
  }


  changeFavoriteStatus(blogID): void {
    let currentUserString = window.localStorage.getItem('currentUser');
    const currentUserObject = JSON.parse(currentUserString);
    currentUserObject.favorites[blogID] =
      !currentUserObject.favorites[blogID];
    currentUserString = JSON.stringify(currentUserObject);
    window.localStorage.setItem('currentUser', currentUserString);
    this.blogService.updateUser(currentUserObject)
      .subscribe((data) => {
      });
  }
}
