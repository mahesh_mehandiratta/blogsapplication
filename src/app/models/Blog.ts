export interface IBlog {
  id: string;
  title: string;
  author: string;
  authorID: string;
  date: string;
  logo: string;
  content: string;
  category: string;
}
